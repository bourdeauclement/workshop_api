const Sequelize = require('sequelize');
const sequelize = require('../services/bddconnection');

const User = sequelize.define('utilisateur', {
  nom: {
    type: Sequelize.STRING
  },
  prenom: {
    type: Sequelize.STRING
  },
  adresse: {
    type: Sequelize.STRING
  },
  note: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  }, mail: {
    type: Sequelize.STRING,
    allowNull: false
  }, password: {
    type: Sequelize.STRING,
    allowNull: false
  }
}, {
  freezeTableName: true,
  timestamps: false
});