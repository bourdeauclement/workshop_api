const Sequelize = require('sequelize');
const sequelize = require('../services/bddconnection');

 sequelize.define("livraisonPanier", {}, {
    freezeTableName: true,
    timestamps: false
});

sequelize.models.livraison.belongsToMany(sequelize.models.panier, {through: 'livraisonPanier'});
sequelize.models.panier.belongsToMany(sequelize.models.livraison, {through: 'livraisonPanier'});