const Sequelize = require('sequelize');
const sequelize = require('../services/bddconnection');

const produit = sequelize.define("produit", {
    nom: {
        type: Sequelize.STRING,
        allowNull: false
    }, commentaire: {
        type: Sequelize.STRING
    }, recupere: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
    }
}, {
    freezeTableName: true,
    timestamps: false
})

produit.belongsTo(sequelize.models.panier, {as : "panier"});