const Sequelize = require('sequelize');
const sequelize = require('../services/bddconnection');

const livraison = sequelize.define('livraison', {
    etatLivraison: {
        type: Sequelize.STRING,
        allowNull: false,
        defaultValue: 'a livrer'
    },dateLivraison : {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    },magasinId: {
        type: Sequelize.STRING,
        allowNull: false
    }
},{
    freezeTableName: true,
    timestamps: false
})

livraison.belongsTo(sequelize.models.utilisateur, {as : "user"});

