const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = require('../services/bddconnection');
var randomString = require("randomstring");

class logToken extends Model {
    static tokenGeneration(size) {
        return(randomString.generate(size));
    }
}

logToken.init({
    token: {
        type: Sequelize.STRING,
        allowNull: false
    }, dateCreation: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    }, dateFin: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW 
    }
}, {
    sequelize,
    freezeTableName: true,
    timestamps: false,
})

sequelize.models.logToken.belongsTo(sequelize.models.utilisateur, { as: "user" });