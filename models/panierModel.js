const Sequelize = require('sequelize');
const sequelize = require('../services/bddconnection');

const panier = sequelize.define("panier", {
    nom: {
        type: Sequelize.STRING,
        allowNull: false
    },
    dateCreation: {
        type: Sequelize.DATE,
        allowNull: false,
        defaultValue: Sequelize.NOW
    }
}, {
    freezeTableName: true,
    timestamps: false
});

panier.belongsTo(sequelize.models.utilisateur, { as: "user" });