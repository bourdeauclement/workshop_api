const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");

const sequelize = require("./services/bddconnection");
require('./services/modelsService');

app.use(cors());
app.use(bodyParser.json());
app.use("/user", require("./routes/userRoute"));
app.use("/logToken", require("./routes/logTokenRoute"));
app.use("/panier", require("./routes/panierRoute"));
app.use("/livraison", require("./routes/livraisonRoute"));

//Connection à la base de données
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

/**
 * Route de base de l'api
 */
app.get("/", (req, res) => {
  res.json({
    message: "Bienvenue sur notre API",
    title: "Workshop api"
  })
});

const port = process.env.PORT || 80;
app.listen(port, '0.0.0.0', () => {
  console.log(`listening on ${port}`);
});
