const router = require("express").Router();
const panierService = require("../services/panierService");

router.post("/add", (req, res) => {
    panierService.add(req.body, (status, response) => {
        res.status(status).json(response);
    })
})

router.get("/search", (req, res) => {
    panierService.findBy(req.query, (status, response) => {
        console.log(response);
        res.status(status).json(response);
    })
})

module.exports = router;