const router = require("express").Router();
const userService = require('../services/userService');

router.get("", (req, res) => {
    userService.findAll(req, (status, response) => {
        console.log(status);
        res.status(status).json({message : response});
    })
})

router.get("/search", (req, res) => {
    userService.findBy(req.query, (status, response) => {
        res.status(status).json(response);
    })
})

router.get("/search/:id", (req, res) => {
    userService.findOneBy(req.params, (status, response) => {
        res.status(status).json(response);
    })
})

router.post("/add", (req,res) => {
    userService.add(req.body, (status, response) => {
        res.status(status).json(response);
    });    
})

router.post("/login", (req,res) => {
    userService.login(req.body, (status, response) => {
        res.status(status).json(response);
    });
})

router.delete("/delete/:id", (req, res) => {
    userService.supprimer(req.params, (status, response) => {
        res.status(status).json(response);
    })
})

router.put("/update/:id", (req, res) => {
    userService.modifier(req.body, Object.values(req.params)[0], (status, response) => {
        res.status(status).json(response);
    })
})

router.get("/store/:magasinId", (req, res) => {
    userService.findByStore(req.params, (status, response) => {
        res.status(status).json(response);
    })
})

module.exports = router;
