const router = require("express").Router();
const livraisonService = require('../services/livraisonService');

router.post("/add", (req, res) => {
    livraisonService.add(req.body, (status, response) => {
        res.status(status).json(response);
    });
})

router.get("/search", (req, res) => {
    livraisonService.findBy(req.query, (status, response) => {
        res.status(status).json(response);
    })
})

router.get("/search/:magasinId", (req, res) => {
    livraisonService.findByStore(req.params, (status, response) => {
        res.status(status).json(response);
    })
})

router.post('/book', (req, res) => {
    livraisonService.reservation(req.body, (status, response) => {
        res.status(status).json(response);
    })
})

router.get('/:livraisonId', (req, res) => {
    livraisonService.userReservations(req.params, (status, response) => {
        res.status(status).json(response);
    })
})

module.exports = router;