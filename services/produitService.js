const sequelize = require('./bddconnection');

/**
 * ajout d'un produit
 * @param {*} req produit a ajouter 
 * @param {*} res reponse de la requete
 */
const add = (req, res) => {
    sequelize.models.produit.create(req);
}

exports.add = add;