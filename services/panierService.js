const sequelize = require('./bddconnection');
const produitService = require('./produitService');

/**
 * Ajout d'un panier
 * @param {*} req le panier a ajouter 
 * @param {*} res le resultat de la requete
 */
const add = (req, res) => {
    sequelize.models.panier.create({
        nom: req.nom,
        userId: req.userId
    }).then(panier => {
        for (let [key, produit] of Object.entries(req.articles)) {
            produit.panierId = panier.dataValues.id;
            produitService.add(produit, response => {
            })
        }
        res(201, "Panier créé")

    }).catch(err => {
        console.log(err);
        res(404, {message : "Panier non créé"})
    })
}

/**
 * Recherche de paniers
 * @param {*} req les critères de recherche
 * @param {*} res le résultat de la recherche
 */
const findBy = (req, res) => {
    sequelize.models.panier.findAll({
        raw : true,
        rejectOnEmpty: true,
        where : req
    }).then( livraisons => {
        res(200, livraisons);
    }).catch( err => {
        console.error('Unable to fin any cart:', err);
        res(404, {message: "Panier non trouvée"}) 
    })
}

exports.add = add;
exports.findBy = findBy;