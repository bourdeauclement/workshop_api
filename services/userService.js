const sequelize = require('./bddconnection');


/**
 * Recherche de tous les utilisateurs
 * @param {*} req 
 * @param {*} res la liste des utilisateurs retournés
 */
const findAll = (req, res) => {
    const User = sequelize.models.utilisateur;
    User.findAll().then(users => {
        res(200, users) ;
    })
        .catch(err => {
            console.error('Unable to fin any user:', err);
            res(404, { message: "Users not found" });
        });
}

/**
 * Recherche d'utilisateurs
 * @param {*} req la requete de recherche
 * @param {*} res les utilisateurs trouvés
 */
const findBy = (req, res) => {
    sequelize.models.utilisateur.findAll({
        raw : true,
        rejectOnEmpty: true,
        where : req
    }).then(user => {
        res(200, user);
    }).catch(err => {
        console.error('Unable to fin any user:', err);
        res(404, {message: "User non trouvé"})  
    })
}

/**
 * Retourne un seul utilisateur correspondant aux critères
 * @param {*} req la requete de recherche
 * @param {*} res l'utilisateur trouvé
 */
const findOneBy = (req, res) => {
    sequelize.models.utilisateur.findOne({
        raw : true,
        rejectOnEmpty: true,
        where : req,
        attributes : ["id","nom","prenom","mail","adresse","note"]
    }).then(user => {
        res(200, user) ;
    }).catch(err => {
        console.error('Unable to fin any user:', err);
        res(404, {message: "User non trouvé"})  
    })
}

/**
 * Création d'un utilisateur
 * @param {*} req les données de l'utilisateur
 * @param {*} res l'utilisateur créé
 */
const add = (req, res) => {
    const User = sequelize.models.utilisateur;
    User.create(req).then(user => {
        res(201,user);
    }).catch(err => {
        console.error('Unable to fin any user:', err);
        res(404, { message: "Utilisateur non créé" });
    });
}

/**
 * Récupère le mot de passe d'un utilisateur
 * @param {*} req 
 * @param {*} res 
 */
const getPassword = (req, res) => {
    sequelize.models.utilisateur.findOne({
        raw : true,
        rejectOnEmpty: true,
        where : req,
        attributes : ["password"]
    }).then(user => {
        res(user);
    }).catch(err => {
        console.log(err);
        res(404, {message : "utilisateur non trouvé"});
    })
}

/**
 * Connection d'un utilisateur
 * @param {*} req mail et mot de passe de l'utilisateur
 * @param {*} res le token créé et certaines données de l'utilisateur connecté
 */
const login = (req, res) => {
    const token = sequelize.models.logToken.tokenGeneration(7);
    findOneBy({mail : req.mail }, (status, response) => {
        if(status == 200){
            getPassword(response.id, password => {
                console.log(password.password);
                if(req.password == password.password){
                    sequelize.models.logToken.create({
                        token : token,
                        userId : response.id
                    }).then(logToken => {
                        res(201,logToken);
                    }).catch(err => {
                        console.error('impossible de créer un logtoken:', err);
                        res(404, { message: "logToken non créé" });
                    });
                }else{
                    res(404, {message : "Mot de passe incorrect"});
                }  
            })                      
        }else{
            res(status,response);
        }
    })
}

/**
 * Supprime un utilisateur
 * @param {*} req l'id de l'utilisateur a supprimer
 * @param {*} res la reponse de la requete
 */
const supprimer = (req, res) => {
    sequelize.models.utilisateur.destroy({
        where : req
    }).then (() => {
        res(200, {message : "utilisateur supprimé"})
    }).catch( err => {
        console.log(err);
        res(400, {message: "erreur de suppression"})
    })
}

/**
 * Modifie un utilisateur
 * @param {*} req l'utilisateur a modifier
 * @param {*} res la reponse de la requete
 */
const modifier = (user, id, res) => {
    sequelize.models.utilisateur.update(user,{
        where : {
            id : id
        }, validate : true
    }).then ( (rows) => {
        if(rows > 0){
            res(200, user)
        }else{
            res(404, {message : "utilisateur non modifié"});
        }        
    }).catch(err => {
        console.log(err);
        res(404, {message : "utilisateur non modifié"});
    })
}




exports.findAll = findAll;
exports.findBy = findBy;
exports.findOneBy = findOneBy;
exports.add = add;
exports.login = login;
exports.supprimer = supprimer;
exports.modifier = modifier;