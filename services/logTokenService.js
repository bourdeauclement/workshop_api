const sequelize = require('./bddconnection');
const User = require('./userService');

/**
 * Recherche d'un logToken en fonction d'un utilisateur
 * @param {*} req critères de recherche d'un utilisateur
 * @param {*} res le log token correspondant
 */
const getLogToken = (req, res) => {
    User.findOneBy(req, (status, response) => {
        if(status == 200){
            sequelize.models.logToken.findOne({
                raw : true,
                where : {
                    userId : response.id
                }                
            }).then(logToken => {
                res(200,logToken);
            }).catch(err => {
                console.error('Unable to fin any user:', err);
                res(404, { message: "logToken non trouvé" });
            });
        }
    })
}

exports.getLogToken = getLogToken;