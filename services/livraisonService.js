const sequelize = require('./bddconnection');

/**
 * Ajoute une livraison
 * @param {*} req 
 * @param {*} res 
 */
const add = (req, res) => {
    sequelize.models.livraison.create(req).then( livraison => {
        res(201, livraison);
    }).catch(err => {
        console.error('Livraison non créée:', err);
        res(404, { message: "Livraison non créée" });
    })
}

/**
 * Recherche de livraisons
 * @param {*} req les critères de recherche
 * @param {*} res le résultat de la recherche
 */
const findBy = (req, res) => {
    sequelize.models.livraison.findAll({
        raw : true,
        rejectOnEmpty: true,
        where : req
    }).then( livraisons => {
        res(200, livraisons);
    }).catch( err => {
        console.error('Unable to fin any delivery:', err);
        res(404, {message: "Livraison non trouvée"}) 
    })
}

/**
 * recherche d'un utilisateur par magasin
 * @param {*} req les données du magasin
 * @param {*} res le résultat de la requete avec les livraisons et les utilisateurs
 */
const findByStore = (req, res) => { 
    sequelize.models.livraison.findAll({
        raw : true,
        rejectOnEmpty: true,
        where : req,
        include: {
            model : sequelize.models.utilisateur,
            as: "user",
            attributes: ["nom", "prenom","note"]
        }
    }).then(livraisons => {
        var aEnvoyer = [];
        livraisons.forEach(item => {
            let object = { 
                id : item.id,
                etatLivraison : item.etatLivraison,
                dateLivraison : item.dateLivraison,
                magasinId : item.magasinId,
                user : {
                    id : item.userId,
                    nom : item["user.nom"],
                    prenom : item["user.prenom"],
                    note : item["user.note"]
                }
            }
            aEnvoyer.push(object);
        })
        res(200, aEnvoyer);
    }).catch(err => {
        console.log(err);
        res(404, {message : "livraison non trouvée"});
    })
}

/**
 * lie un panier à une livraison lors d'un réservation
 * @param {*} req l'id de la livraison et du panier
 * @param {*} res le résultat de la requete
 */
const reservation = (req , res) => {
    sequelize.models.livraisonPanier.create(
        req
    ).then( livraisonPanier => {
        res(201, livraisonPanier);
    }).catch(err => {
        console.log(err);
        res(404, {Message : "Livraison et panier non associés"});
    })
}

/**
 * Liste des livraisons actives pour un utilisateur
 * @param {*} req id de l'utilisateur
 * @param {*} res résultat de la requete
 */
const userReservations = (req, res) => {
    sequelize.query('SELECT panier.id as panierId, panier.nom as panierNom, u2.prenom as userPrenom FROM panier INNER JOIN livraisonPanier ON panier.id = livraisonPanier.panierId INNER JOIN livraison ON livraisonPanier.livraisonId=livraison.id INNER JOIN utilisateur as u2 ON panier.userId = u2.id WHERE livraison.id= :id',
    {
        replacements : {id : req.livraisonId},
        raw : true,
        rejectOnEmpty: true,
        type: sequelize.QueryTypes.SELECT
    }).then(paniers => {
        console.log(paniers);
        res(200, paniers);
    }).catch(err => {
        console.log(err);
        res(404, {message : "Aucune livraison en attente"});
    })
}

exports.add = add;
exports.findBy = findBy;
exports.findByStore = findByStore;
exports.reservation = reservation;
exports.userReservations = userReservations;